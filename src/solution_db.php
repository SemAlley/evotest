<?php
require_once 'app/autoload.php';

/**
 * Скрипт для поиска пользователей с файла csv средствами php + mysql
 * Вывод в консоль [uid, date] найденых результатов
 * Если под условие поиска uid попадает больше 1 раза, то береться последний по дате
 */
(new class
{
    /**
     * @var $startTime DateTime
     */
    private $startTime;
    private $dbTableName = 'transactions';

    private $sum;

    public function run($argv)
    {
        $this->start();
        $fileName = $argv[1] ?? null;
        $this->sum = $argv[2] ?? null;

        if (!$fileName || !$this->sum) {
            $this->stop('Not all required parameters added');
        }

        $reader = new \app\Reader\CsvReader(__DIR__ . '/' . $fileName);

        $connection = $this->getConnection();

        // Clear table before store new data
        $sql = "TRUNCATE TABLE `{$this->dbTableName}`";
        $statement = $connection->prepare($sql);
        $statement->execute();

        // Store data from csv
        $i = 0;
        $dateSet = [];
        $headers = ['uid', 'date', 'sum'];
        foreach ($reader->iterate() as $row) {
            $i++;
            $dateSet[] = $row;
            if (($i % 500) === 0) {
                $this->batchInsert($connection, $dateSet, $headers);
                $dateSet = [];
                $i = 0;
            }
        }
        // Insert last part
        if (!empty($dateSet)) {
            $this->batchInsert($connection, $dateSet, $headers);
        }

        $this->track('data imported');

        $searchQuery = "SELECT DISTINCT `uid`, `date` FROM {$this->dbTableName} WHERE `sum` >= ? ORDER BY `date`";
        $statement = $connection->prepare($searchQuery);
        $statement->execute([$this->sum]);
        while ($item = $statement->fetch(PDO::FETCH_BOTH)) {
//            echo implode(', ', $item) . "\n";
        }

        $this->stop('The end');
    }

    /**
     * Retrieve db connection and create table if not exists
     *
     * @return PDO
     */
    private function getConnection(): PDO
    {
        try {
            $db = new PDO('mysql:host=db;dbname=evo', 'root', 'root');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $db->prepare("SHOW TABLES LIKE '{$this->dbTableName}'");
            $stmt->execute();
            if (!$stmt->fetch()) {
                $sql = "CREATE table {$this->dbTableName}(
                 id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                 uid VARCHAR( 50 ) NOT NULL, 
                 sum FLOAT,
                 date TIMESTAMP)";
                $db->exec($sql);
                $db->exec("CREATE  INDEX idx_evo_sum_date ON {$this->dbTableName}(sum, date);");
                echo "Created '{$this->dbTableName}' table.\n";
            }

        } catch (PDOException $e) {
            unset($db);
            $this->stop($e->getMessage());
        }

        return $db;
    }

    private function batchInsert(PDO $connection, array $rows, array $columns = [])
    {
        $columnCount = !empty($columns) ? count($columns) : count(reset($rows));
        $columnList = !empty($columns) ? '(' . implode(', ', $columns) . ')' : '';
        $rowPlaceholder = ' (' . implode(', ', array_fill(1, $columnCount, '?')) . ')';
        // Build the whole prepared query
        $query = sprintf(
            'INSERT INTO %s%s VALUES %s',
            $this->dbTableName,
            $columnList,
            implode(', ', array_fill(1, count($rows), $rowPlaceholder))
        );
        $connection->beginTransaction();
        try {
            // Prepare PDO statement
            $statement = $connection->prepare($query);
            // Flatten the value array (we are using ? placeholders)
            $data = [];
            foreach ($rows as $rowData) {
                foreach ($rowData as $rowField) {
                    $data[] = $rowField;
                }
            }
            $statement->execute($data);
            $connection->commit();
        } catch (PDOException $e) {
            $connection->rollBack();
            unset($connection);
            $this->stop($e->getMessage());
        }
    }

    private function start(): void
    {
        $this->startTime = new DateTime();
    }

    private function track($msg): void
    {
        $diff = (new DateTime())->getTimestamp() - $this->startTime->getTimestamp();
        echo "************************************ \n";
        echo "$msg \n";
        echo "Script works - $diff seconds \n";
        echo "************************************ \n";
    }

    private function stop($msg): void
    {
        $diff = (new DateTime())->getTimestamp() - $this->startTime->getTimestamp();
        echo "$msg \n";
        echo "************************************ \n";
        echo "Script works - $diff seconds \n";
        die();
    }
})->run($argv);
<?php
require_once 'app/autoload.php';

/**
 * Скрипт для поиска пользователей с файла csv средствами php
 * Вывод в консоль [uid, date] найденых результатов
 * Если под условие поиска uid попадает больше 1 раза, то береться последний по дате
 */
(new class
{
    /**
     * @var $startTime DateTime
     */
    private $startTime;

    private $sum;

    public function run($argv)
    {
        $this->start();
        $fileName = $argv[1] ?? null;
        $this->sum = $argv[2] ?? null;

        if (!$fileName || !$this->sum) {
            $this->stop('Not all required parameters added');
        }

        $reader = new \app\Reader\CsvReader(__DIR__ . '/' . $fileName);

        $result = [];
        foreach ($reader->iterate() as $row) {
            if (!($itemSum = $row['sum'] ?? null)) continue;
            if ((float)$itemSum < (float)$this->sum) continue;
            $existed = $result[$row['uid']] ?? null;
            if ($existed && (strtotime($existed['date']) - strtotime($row['date'])) > 0) continue;
            $result[$row['uid']] = [$row['uid'], $row['date']];
        }

        foreach ($result as $item) {
            echo implode(', ', $item) . "\n";
        }

        $this->stop('');
    }

    private function start(): void
    {
        $this->startTime = new DateTime();
    }

    private function stop($msg): void
    {
        $diff = (new DateTime())->getTimestamp() - $this->startTime->getTimestamp();
        echo "$msg \n";
        echo "************************************ \n";
        echo "Script works - $diff seconds \n";
        die();
    }
})->run($argv);
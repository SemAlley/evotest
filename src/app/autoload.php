<?php
/**
 * Most powerful autoload you ever see
 */
spl_autoload_register(function ($class) {
    $parts = explode('\\', $class);
    array_shift($parts);
    $classPath = __DIR__ . '/' . implode('/', $parts) . '.php';
    if(!file_exists($classPath)) throw new Exception("No such file [$classPath]");
    include_once $classPath;
});
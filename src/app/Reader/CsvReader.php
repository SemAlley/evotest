<?php
/**
 * Created by PhpStorm.
 * User: semal
 * Date: 2/27/2019
 * Time: 1:46 PM
 */

namespace app\Reader;

use app\Exceptions\FileNotExists;

/**
 * Class Reader
 * @package app\Reader
 */
class CsvReader
{
    /**
     * @var string Path for fole to read
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        if (!file_exists($filePath)) throw new FileNotExists("File [$filePath] doesnt exists");
        $this->filePath = $filePath;
    }

    /**
     * Generator for file read
     *
     * @return \Iterator
     */
    public function iterate(): \Iterator
    {
        if (($handle = fopen($this->filePath, 'r')) !== FALSE) {
            $header =  fgetcsv($handle);
            while (($row = fgetcsv($handle)) !== FALSE) {
                if (count($row) > 0) {
                    yield array_combine($header, $row);
                }
            }

            fclose($handle);
        }
    }
}
# Тестовое задание для *evoplay*

## Запуск

- Запускаем докер
```
docker-compose build
docker-compose up
```

- Запуск скриптов

решение с помощью чистого php

```
docker-compose exec php php solution.php dataset-big.csv 2000
docker-compose exec php php solution.php dataset-small.csv 25
```


решение с помощью php + mysql

```
docker-compose exec php php solution_db.php dataset-big.csv 2000
docker-compose exec php php solution_db.php dataset-small.csv 25
```
